#!/bin/bash

URL=http://api.openweathermap.org/data/2.5/forecast
API_KEY=036e08a38366fc08b4a16a15345da529
UNITS=metric
CITY_ID=8133886 #Glyfada - http://bulk.openweathermap.org/sample/city.list.json.gz

WEATHER_DATA_URL="${URL}?id=${CITY_ID}&appid=${API_KEY}&units=${UNITS}"

wget -q -O /tmp/weatherdata.json $WEATHER_DATA_URL

VRESOLUTION=`xdpyinfo | grep dimensions | awk '{print $2}' | cut -d'x' -f2`

ITERATOR=5 #by default show 5 weather entries

if [ -n "$VRESOLUTION" ] && [ "$VRESOLUTION" -ge '1080' ]; then
  ITERATOR=10 #show 10 weather entries if vertical resolution is >=1080
fi

if [ $? -eq 0 ]; then
  echo "Date          Time     Temp    Feels   Desc"
  for (( i=0; i<$ITERATOR; i++ )); do
    RAWDATA=$(cat /tmp/weatherdata.json | jq ".list[$i].dt_txt, .list[$i].main.temp, .list[$i].main.feels_like, .list[$i].weather[0].main")  
    DATE_RAW=`echo $RAWDATA | awk '{print $1}' | sed 's/\"//'`
    TIME_RAW=`echo $RAWDATA | awk '{print $2}'`
    TEMP_RAW=`echo $RAWDATA | awk '{print $3}'`
    FEELS_LIKE_RAW=`echo $RAWDATA | awk '{print $4}'`
    DESCR_FORMATTED=`echo $RAWDATA | awk '{print $NF}' | sed 's/\"//g'`
    DATE_FORMATTED=`echo $DATE_RAW | awk -F'-' '{print $3"-"$2"-"$1}'`
    TIME_FORMATTED=`echo $TIME_RAW | awk -F':' '{print $1":"$2}'`
    TEMP_FORMATTED=`echo $TEMP_RAW | awk -F'.' '{print $1"°C"}'`
    FEELS_LIKE_FORMATTED=`echo $FEELS_LIKE_RAW | awk -F'.' '{print $1"°C"}'`

    echo "$DATE_FORMATTED    $TIME_FORMATTED    $TEMP_FORMATTED    $FEELS_LIKE_FORMATTED    $DESCR_FORMATTED"


  done 
  
else 
  exit 1
fi
