#!/bin/bash 

export MAIN_DIALOG=' 

<window title="Hot Keys" icon-name="gtk-about" resizable="true" name="hotkeys"> 

<vbox> 
<text use-markup="true" xalign="0"><label>"<b>Alt + 1...4</b>  : Go to virtual desktop 1...4"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Alt + t</b>      : Open Text Editor"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Alt + f</b>      : Open File Manager"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Alt + enter</b>  : Open Ternimal"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Alt + m</b>      : Show Minimal Menu"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Alt + d</b>      : Show App Launcher"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Alt + F12</b>    : Show Hot Keys"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Alt + PrtScn</b> : Take screenshot of a selected area"</label></text>
<text use-markup="true" xalign="0"><label>"<b>PrtScn</b>       : Take screenshot of the entire desktop"</label></text>

<text><label>""</label></text>

<text use-markup="true" xalign="0"><label>"<b>Ctrl + Alt + d</b> : Toggle Desktop"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Ctrl + Alt + l</b> : Lock Desktop"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Ctrl + Alt + x</b> : Exit Desktop (logout)"</label></text>
<text use-markup="true" xalign="0"><label>"<b>Ctrl + Alt + s</b> : Put Desktop for sleep....ZZzzzzzz"</label></text>

<text><label>""</label></text>

<text use-markup="true" xalign="0" width-request="930"><label>"<b>Windows key + left arrow</b>  : Active window fills left portion of the screen"</label></text>
<text use-markup="true" xalign="0" width-request="930"><label>"<b>Windows key + right arrow</b> : Active window fills right portion of the screen "</label></text>
<text use-markup="true" xalign="0" width-request="930"><label>"<b>Windows key + up arrow</b>    : Active window fills top portion of teh screen"</label></text>
<text use-markup="true" xalign="0" width-request="930"><label>"<b>Windows key + down arrow</b>  : Active window fills bottom portion of the screen"</label></text>
<text use-markup="true" xalign="0" width-request="930"><label>"<b>Windows key + space bar</b>   : Active window is maximized, i.e fills the entire screen"</label></text>
</vbox> 
</window> 
' 
gtkdialog --program=MAIN_DIALOG
