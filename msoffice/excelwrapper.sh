#!/bin/bash

# if any file is passed as argument, pass it to word. Otherwise open word without passing any arguments
if [ -n "$*" ]; then

  # fix file path e.g /tmp/file.doc -> Z:\\tmp\file.doc
  FILENAME="$(echo "$*" | sed 's/\//\\\\/g')"
  env WINEPREFIX=~/.wine /opt/wine4/bin/wine ~/.wine/drive_c/Program\ Files/Microsoft\ Office/Office12/EXCEL.EXE "Z:$FILENAME"
else
  env WINEPREFIX=~/.wine /opt/wine4/bin/wine ~/.wine/drive_c/Program\ Files/Microsoft\ Office/Office12/EXCEL.EXE
fi
