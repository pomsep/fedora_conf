#!/bin/bash

# This script performs post installation actions to configure FEDORA 31 to our taste, replacing GNOME with Openbox, along with other utility apps 

# Constants for colored output
RED='\033[0;31m'
REDB='\033[1;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
NCB='\033[1m' # BOLD No Color

LOG=/root/fedora_custom_config.log

print_service_status() {
	if systemctl list-unit-files --type service | grep -qi $1; then
		local STATUS=`systemctl list-unit-files --type service | grep -i $1 | awk '{print $2}'`
		if [ $STATUS = 'enabled' ]; then
			echo -e "${GREEN}ENABLED${NC}" | tee -a $LOG
		elif [ $STATUS = 'disabled' ] || [ $STATUS = 'masked' ]; then
			echo -e "${RED}DISABLED${NC}" | tee -a $LOG
		fi
	else
		echo -e "${RED}not found. Check this after script execution completes${NC}" | tee -a $LOG
	fi
}


rm $LOG &> /dev/null
touch $LOG

clear

OS_TESTED_VERSION=31
OS_VERSION=`cut -d' ' -f3 /etc/fedora-release`

echo
echo "LOG: $LOG"
echo

echo
if [ $OS_TESTED_VERSION -ne $OS_VERSION ]; then
	echo -e "This script is tested in Fedora ${NCB}v${OS_TESTED_VERSION}${NC}." 
	echo "However, it should work in other versions too. Pls check LOG file in the end, to identify any issues, if something is not working as expected." 
	echo
	#read -p "Press any key to continue..." REPLY
	sleep 3
fi


echo
if [ "$UID" -ne '0' ]; then
	echo -e "${RED}You must run the script as ${REDB}root${RED}. Execution halted ! ${NC}\n" | (tee -a $LOG 2>/dev/null)
	exit 1
fi

echo -e "\n#########################\n" >> $LOG
echo -e "${NCB}1. Checking if normal user has been created:${NC}" | tee -a $LOG

# Check that a normal user is already added to the system, after installation. Normal users usually start from UID 1000
if [ `grep 1000 /etc/passwd | wc -l` -eq 1 ]; then
	USER_NAME=`grep 1000 /etc/passwd | cut -d':' -f1`
	echo -e "${GREEN}Normal user found -> $USER_NAME${NC}" | tee -a $LOG
else
	echo -e "${RED}No normal user found. Execution halted ! ${NC}\n" | tee -a $LOG
	exit 1
fi

USER_HOMEDIR=/home/$USER_NAME


echo -e "\n#########################\n" >> $LOG

# Disabling unecessary services
echo -e "\n${NCB}2. Stopping and disabling unecessary services:${NC}" | tee -a $LOG

SERVICES_TO_DISABLE="firewalld auditd import-state kdump NetworkManager-wait-online nis-domainname selinux-autorelabel-mark"

for SERVICE in $SERVICES_TO_DISABLE; do
	echo "> systemctl stop $SERVICE" >> $LOG
	systemctl stop $SERVICE &>> $LOG

	echo "> systemctl disable $SERVICE" >> $LOG
	systemctl disable $SERVICE &>> $LOG

	if [ $SERVICE = "firewalld" ]; then
		echo "> systemctl mask $SERVICE" >> $LOG
		systemctl mask $SERVICE &>> $LOG
	fi

	echo -n "$SERVICE..." | tee -a $LOG
	print_service_status "$SERVICE"
done

echo -n "disabling SELINUX..." | tee -a $LOG
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config &>> $LOG && (echo -e "${GREEN}OK${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)

sed -i 's/SELINUX=permissive/SELINUX=disabled/' /etc/selinux/config &>> $LOG #just to be on the safe side 

echo -e "\n#########################\n" >> $LOG

# Installing pkgs and repos

echo -e "\n${NCB}3. Downloading & installing packages and 3rd party repos:${NC}" | tee -a $LOG

echo -n "Checking for network..."
ping -c3 4.2.2.2 &>> $LOG && echo -e "${GREEN}OK${NC}" || { echo -e "${RED}FAILED. Internet unreachable. Execution halted ! ${NC}"; exit 1; }

echo -n "Repos..." | tee -a $LOG
# REPOS
echo "yum -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" >> $LOG
yum -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm &>> $LOG

if [ `ls -l /etc/yum.repos.d/rpmfusion-free* 2>> $LOG | wc -l` -eq '0' ]; then
	echo -e "${REDB}RPM FUSION FREE${RED} repo could not be installed. Execution halted ! ${NC}" | tee -a $LOG
	exit 1
fi

echo "yum -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm" >> $LOG
yum -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm &>> $LOG

if [ `ls -l /etc/yum.repos.d/rpmfusion-nonfree* 2>> $LOG | wc -l` -eq '0' ]; then
	echo -e "${REDB}RPM FUSION NON FREE${RED} repo could not be installed. Execution halted ! ${NC}" | tee -a $LOG
	exit 1
fi


echo "rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg" >> $LOG
rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg &>> $LOG

echo "dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo" >> $LOG
dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo &>> $LOG


if [ `ls -l /etc/yum.repos.d/sublime* 2>> $LOG | wc -l` -eq '0' ]; then
	echo -e "${REDB}SUBLIME${RED} repo could not be installed. Execution halted ! ${NC}" | tee -a $LOG
	exit 1
fi

echo -e "${GREEN}OK${NC}" | tee -a $LOG

echo

# PKGS
PKGS_TO_INSTALL="iptables-services openbox obconf network-manager-applet pavucontrol dconf-editor tint2 clipit volumeicon dmenu rofi thunar thunar-volman thunar-archive-plugin catfish lxappearance i3lock terminator git zip vlc audacious audacious-plugins* xrandr arandr light sublime-text google-roboto-* dejavu-* arc-theme glibc.i686 ncurses-compat-libs.i686 samba-winbind-clients libXext.i686 libxml2.i686 wine*fonts freetype.i686 mesa-dri-drivers.i686 wine-capi.i686 wine-cms.i686 wine-common wine-core.i686 wine-fonts wine-ldap.i686 wine-mono wine-openal.i686 wine-opencl.i686 wine-pulseaudio.i686 wine-twain.i686 iftop gtkdialog conky jq lm_sensors xorg-x11-utils atk.i686 gdk-pixbuf2.i686 gdk-pixbuf2-xlib.i686 gtk2.i686 mesa-libGLU.i686 libidn1.34.i686 pangox-compat.i686 libXt.i686 gimp viewnior nmap cups-pdf flowblade"

# atk.i686 gdk-pixbuf2.i686 gdk-pixbuf2-xlib.i686 gtk2.i686 mesa-libGLU.i686 libidn1.34.i686 pangox-compat.i686 libXt.i686 -> Those pkgs are deoendencies of Adobe Reader


echo "Trying to install following pkgs. Pls wait... :" | tee -a $LOG
echo "$PKGS_TO_INSTALL"

echo "yum -y install $PKGS_TO_INSTALL" >> $LOG
yum -y install $PKGS_TO_INSTALL &>> $LOG

echo | tee -a $LOG

for PKG in $PKGS_TO_INSTALL; do 
	[[ $PKG =~ \.i686$ ]] && PKG=`echo $PKG | cut -d'.' -f1` # convert pkgname.i686 -> pkgname to find if it's installed with grep 
	[[ $PKG =~ \*[a-zA-Z] ]] && PKG=`echo $PKG | awk -F'*' '{print $1".*"$2}'` # convert pkgname*lala -> pkgname.*lala to find if it's installed with grep 
	rpm -qa | grep -qi $PKG || echo -e "${REDB}${PKG}${RED} could not be installed ! ${NC}" | tee -a $LOG
done

echo | tee -a $LOG

echo "> systemctl start iptables" >> $LOG
systemctl start iptables &>> $LOG

echo "> systemctl enable iptables" >> $LOG
systemctl enable iptables &>> $LOG

echo -n "Iptables..." | tee -a $LOG
print_service_status "iptables"

echo | tee -a $LOG

# DOWNLOAD MY GIT REPOS
echo "Downloading my repos from BitBucket. This will take a while. Pls wait..." | tee -a $LOG

mkdir $USER_HOMEDIR/git_repos &>> $LOG
cd $USER_HOMEDIR/git_repos &>> $LOG

#wget -q -O conf.zip https://bitbucket.org/pomsep/fedora_conf/get/master.zip 2>> $LOG || { echo -e "${RED}CONF repo was not downloaded successfully. Execution halted ! ${NC}"; exit 1; }
echo 'curl -# -o conf.zip https://bitbucket.org/pomsep/fedora_conf/get/master.zip' >> $LOG
echo
echo "[ 1 / 2 ]"
curl -f -# -o conf.zip https://bitbucket.org/pomsep/fedora_conf/get/master.zip || { echo -e "${RED}CONF repo was not downloaded successfully. Execution halted ! ${NC}"; exit 1; }
unzip conf.zip &>> $LOG || { echo -e "${RED}CONF repo was not uncompressed successfully. Execution halted ! ${NC}"; exit 1; }

#wget -q -O pkgs.zip https://bitbucket.org/pomsep/fedora_pkgs/get/master.zip 2>> $LOG || { echo -e "${RED}PKGS repo was not downloaded successfully. Execution halted ! ${NC}"; exit 1; }
echo 'curl -# -o pkgs.zip https://bitbucket.org/pomsep/fedora_pkgs/get/master.zip' >> $LOG
echo
echo "[ 2 / 2 ]"
curl -f -# -o pkgs.zip https://bitbucket.org/pomsep/fedora_pkgs/get/master.zip || { echo -e "${RED}PKGS repo was not downloaded successfully. Execution halted ! ${NC}"; exit 1; }
unzip pkgs.zip &>> $LOG || { echo -e "${RED}PKGS repo was not uncompressed successfully. Execution halted ! ${NC}"; exit 1; }

mv $USER_HOMEDIR/git_repos/pomsep-fedora_conf* $USER_HOMEDIR/git_repos/fc31_conf &>> $LOG || { echo -e "${RED}CONF repo was not renamed successfully. Execution halted ! ${NC}"; exit 1; }
mv $USER_HOMEDIR/git_repos/pomsep-fedora_pkgs* $USER_HOMEDIR/git_repos/pkgs &>> $LOG || { echo -e "${RED}PKGS repo was not renamed successfully. Execution halted ! ${NC}"; exit 1; }

chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/git_repos &>> $LOG

echo

# ICON THEME
echo -n "Icon theme..." | tee -a $LOG
echo '> wget -qO- https://raw.githubusercontent.com/gusbemacbe/suru-plus/master/install.sh | sudo env DESTDIR="/usr/share/icons" sh' >> $LOG
wget -qO- https://raw.githubusercontent.com/gusbemacbe/suru-plus/master/install.sh | env DESTDIR="/usr/share/icons" sh &>> $LOG

if [ `ls -l /usr/share/icons/ 2>> $LOG | grep -i suru | wc -l` -eq '0' ]; then 
	echo -e "${RED}FAILED${NC}" | tee -a $LOG
else
	echo -e "${GREEN}DONE${NC}" | tee -a $LOG
fi


# OPENBOX THEME
echo -n "Openbox theme..." | tee -a $LOG
mkdir $USER_HOMEDIR/.themes &>> $LOG

echo '> git clone https://github.com/addy-dclxvi/openbox-theme-collections $USER_HOMEDIR/.themes' >> $LOG
git clone https://github.com/addy-dclxvi/openbox-theme-collections $USER_HOMEDIR/.themes &>> $LOG

echo '> rm -r $USER_HOMEDIR/.themes/.git' >> $LOG
rm -r $USER_HOMEDIR/.themes/.git &>> $LOG

echo '> chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.themes' >> $LOG
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.themes &>> $LOG


if [ `ls -l $USER_HOMEDIR/.themes 2>> $LOG | grep -i raven | wc -l` -eq '0' ]; then 
	echo -e "${RED}FAILED${NC}" | tee -a $LOG
else
	echo -e "${GREEN}DONE${NC}" | tee -a $LOG
	# change the color of min,max,close buttons of inactive window to be more visible
	sed -i "/^window.inactive.button.unpressed.image.color/c\window.inactive.button.unpressed.image.color: #8b8f93" $USER_HOMEDIR/.themes/Raven-Cyan/openbox-3/themerc &>> $LOG
fi


echo -e "\n#########################\n" >> $LOG


# Configuration
echo -e "\n${NCB}4. Configuring rest system:${NC}" | tee -a $LOG

echo -n "Setting up GR keyboard layout in virtual console..." | tee -a $LOG
localectl set-keymap us gr --no-convert &>> $LOG
setfont gr737a-9x16 &>> $LOG
systemctl restart systemd-vconsole-setup &>> $LOG

echo "KEYMAP=us
FONT=gr737a-9x16
KEYMAP_TOGGLE=gr" > /etc/vconsole.conf && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)


echo -n "Setting up GR keyboard layout in X..." | tee -a $LOG
localectl set-x11-keymap us,gr “” “” “grp:alt_shift_toggle” --no-convert &>> $LOG

echo 'Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "us,gr"
        Option "XkbOptions" "grp:alt_shift_toggle"
EndSection' > /etc/X11/xorg.conf.d/00-keyboard.conf && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)


echo | tee -a $LOG

echo "Setting default apps..." | tee -a $LOG


#sed -i 's/org.gnome.gedit.desktop/sublime_text.desktop/g' /usr/share/applications/mimeapps.list &>> $LOG
MIME_TYPES=`grep -i gedit.desktop /usr/share/applications/mimeapps.list | cut -d= -f1`
if [ -n "$MIME_TYPES" ]; then
	for MIME in $MIME_TYPES; do
		su - $USER_NAME -c "gio mime $MIME sublime_text.desktop" &>> $LOG
	done
fi
echo "TEXT EDITOR -> sublime-text" | tee -a $LOG


#sed -i 's/eog.desktop/viewnior.desktop/g' /usr/share/applications/mimeapps.list &>> $LOG
MIME_TYPES=`grep -i eog.desktop /usr/share/applications/mimeapps.list | cut -d= -f1`
if [ -n "$MIME_TYPES" ]; then
	for MIME in $MIME_TYPES; do
		su - $USER_NAME -c "gio mime $MIME viewnior.desktop" &>> $LOG
	done
fi
echo "PHOTO VIEWER -> viewnior" | tee -a $LOG


#sed -i 's/org.gnome.Nautilus.desktop/thunar.desktop/g' /usr/share/applications/mimeapps.list &>> $LOG
MIME_TYPES=`grep -i nautilus.desktop /usr/share/applications/mimeapps.list | cut -d= -f1`
if [ -n "$MIME_TYPES" ]; then
	for MIME in $MIME_TYPES; do
		su - $USER_NAME -c "gio mime $MIME thunar.desktop" &>> $LOG
	done
fi
echo "FILE MANAGER -> thunar" | tee -a $LOG


#sed -i 's/org.gnome.Totem.desktop/vlc.desktop/g' /usr/share/applications/mimeapps.list &>> $LOG
MIME_TYPES=`grep -i totem.desktop /usr/share/applications/mimeapps.list | cut -d= -f1`
if [ -n "$MIME_TYPES" ]; then
	for MIME in $MIME_TYPES; do
		su - $USER_NAME -c "gio mime $MIME vlc.desktop" &>> $LOG
	done
fi
echo "MEDIA PLAYER -> vlc" | tee -a $LOG


#sed -i 's/org.gnome.Rhythmbox3.desktop/audacious.desktop/g' /usr/share/applications/mimeapps.list &>> $LOG
#sed -i 's/rhythmbox.desktop/audacious.desktop/g' /usr/share/applications/mimeapps.list &>> $LOG
MIME_TYPES=`grep -i rhythmbox /usr/share/applications/mimeapps.list | cut -d= -f1`
if [ -n "$MIME_TYPES" ]; then
	for MIME in $MIME_TYPES; do
		su - $USER_NAME -c "gio mime $MIME audacious.desktop" &>> $LOG
	done
fi
echo "MUSIC PLAYER -> audacious" | tee -a $LOG

# This will be done later in the script but we show it here anyway
echo "PDF READER -> Adobe" | tee -a $LOG

# remove "OnlyShowIn=GNOME;KDE;LXDE;MATE;Razor;ROX;TDE;Unity;XFCE;" line so that "parcellite" also shows up in openbox
#echo | tee -a $LOG
#echo -n "Fixing 'parcellite (clipboard)' to launch at startup..." | tee -a $LOG
#sed -i '/OnlyShowIn/Id' /etc/xdg/autostart/parcellite-startup.desktop &>> $LOG
#
#if [ -f /etc/xdg/autostart/parcellite-startup.desktop ]; then
#	grep -qi onlyshowin /etc/xdg/autostart/parcellite-startup.desktop && (echo -e "${RED}FAILED${NC}" | tee -a $LOG) || (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) 
#fi


# change the color of directories when doing 'ls'
echo 'export LS_COLORS=$LS_COLORS"di=1;35:"' >> /etc/bashrc 

# remove nautilus
echo "yum -y remove nautilus" >> $LOG
yum -y remove nautilus &>> $LOG

# remove single quotes when displaying space separated filenames/foldernames with 'ls'
touch /etc/profile.d/unquote_strings.sh &>> $LOG
echo export QUOTING_STYLE=literal > /etc/profile.d/unquote_strings.sh


# copy configuration files for OPENBOX, TINT2, GTK3 etc..
echo >> $LOG
echo "mkdir $USER_HOMEDIR/git_repos" >> $LOG
mkdir $USER_HOMEDIR/git_repos &>> $LOG

# assuming the following paths after pulling from git
# $USER_HOMEDIR/git_repos/pkgs/
#		> FoxitReader.enu.setup.2.4.4.0911.x64.run.tar.gz
#		> google-chrome-stable_current_x86_64.rpm
#		> office_2007_enterprise_32bit.tar.gz
#		> rarlinux-x64-5.9.0.tar.gz

# $USER_HOMEDIR/git_repos/fc31_conf/
# 		> configure.sh
# 		> gtk3/
# 		> openbox/
# 		> terminator/
# 		> thunar/0
#		> tint2/
#		> volumeicon/

########### COMMAND TO PULL THE REPOS FROM GIT ##########

chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/git_repos/fc31_conf &>> $LOG

echo

echo -n "Copying GTK3 configuration..." | tee -a $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/gtk3/settings.ini $USER_HOMEDIR/.config/gtk-3.0/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/gtk-3.0 &>> $LOG
chmod 755 $USER_HOMEDIR/.config/gtk-3.0 &>> $LOG
chmod 644 $USER_HOMEDIR/.config/gtk-3.0/* &>> $LOG

echo -n "Copying GTK2 configuration..." | tee -a $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/gtk2/.gtkrc-2.0 $USER_HOMEDIR/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.gtkrc-2.0 &>> $LOG
chmod 644 $USER_HOMEDIR/.gtkrc-2.0 &>> $LOG

echo -n "Copying OPENBOX configuration..." | tee -a $LOG
mkdir $USER_HOMEDIR/.config/openbox &>> $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/openbox/* $USER_HOMEDIR/.config/openbox/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/openbox &>> $LOG
chmod 755 $USER_HOMEDIR/.config/openbox &>> $LOG
chmod 644 $USER_HOMEDIR/.config/openbox/* &>> $LOG
chmod +x $USER_HOMEDIR/.config/openbox/hotkeys.sh &>> $LOG

echo -n "Copying TERMINATOR configuration..." | tee -a $LOG
mkdir $USER_HOMEDIR/.config/terminator &>> $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/terminator/config $USER_HOMEDIR/.config/terminator/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/terminator &>> $LOG
chmod 755 $USER_HOMEDIR/.config/terminator &>> $LOG
chmod 644 $USER_HOMEDIR/.config/terminator/* &>> $LOG

echo -n "Copying THUNAR configuration..." | tee -a $LOG
mkdir $USER_HOMEDIR/.config/Thunar &>> $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/thunar/* $USER_HOMEDIR/.config/Thunar/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/Thunar &>> $LOG
chmod 755 $USER_HOMEDIR/.config/Thunar &>> $LOG
chmod 644 $USER_HOMEDIR/.config/Thunar/* &>> $LOG

echo -n "Copying TINT2 configuration..." | tee -a $LOG
mkdir $USER_HOMEDIR/.config/tint2 &>> $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/tint2/* $USER_HOMEDIR/.config/tint2/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/tint2 &>> $LOG
chmod 755 $USER_HOMEDIR/.config/tint2 &>> $LOG
chmod 644 $USER_HOMEDIR/.config/tint2/* &>> $LOG

echo -n "Copying VOLUMEICON configuration..." | tee -a $LOG
mkdir $USER_HOMEDIR/.config/volumeicon &>> $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/volumeicon/* $USER_HOMEDIR/.config/volumeicon/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/volumeicon &>> $LOG
chmod 755 $USER_HOMEDIR/.config/volumeicon &>> $LOG
chmod 644 $USER_HOMEDIR/.config/volumeicon/* &>> $LOG

echo -n "Copying VIM configuration..." | tee -a $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/vim/.vimrc $USER_HOMEDIR/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown $USER_NAME.$USER_NAME $USER_HOMEDIR/.vimrc &>> $LOG
chmod 644 $USER_HOMEDIR/.vimrc &>> $LOG

echo -n "Copying CONKY configuration..." | tee -a $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/conky/.conkyrc $USER_HOMEDIR/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown $USER_NAME.$USER_NAME $USER_HOMEDIR/.conkyrc &>> $LOG
chmod 644 $USER_HOMEDIR/.conkyrc &>> $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/weather_grabber.sh /usr/local/bin/
chmod 777 /usr/local/bin/weather_grabber.sh
chown $USER_NAME.$USER_NAME /usr/local/bin/weather_grabber.sh

echo -n "Copying ROFI configuration..." | tee -a $LOG
mkdir $USER_HOMEDIR/.config/rofi &>> $LOG
echo rofi.theme: /usr/share/rofi/themes/Pop-Dark.rasi > $USER_HOMEDIR/.config/rofi/config 2> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/rofi &>> $LOG
chmod 755 $USER_HOMEDIR/.config/rofi &>> $LOG
chmod 644 $USER_HOMEDIR/.config/rofi/* &>> $LOG

echo -n "Copying CLIPIT configuration..." | tee -a $LOG
mkdir $USER_HOMEDIR/.config/clipit &>> $LOG
cp -f $USER_HOMEDIR/git_repos/fc31_conf/clipit/* $USER_HOMEDIR/.config/clipit/ &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/clipit &>> $LOG
chmod 755 $USER_HOMEDIR/.config/clipit &>> $LOG
chmod 644 $USER_HOMEDIR/.config/clipit/* &>> $LOG

echo -n "Changing CUPS-PDF storage path to user's documents directory..." | tee -a $LOG
sed -i '/^Out/c\Out ${HOME}/Documents' /etc/cups/cups-pdf.conf &>> $LOG

echo -n "Configuring Sublime Text..." | tee -a $LOG
mkdir -p $USER_HOMEDIR/.config/sublime-text-3/Packages/User/ &>> $LOG
touch $USER_HOMEDIR/.config/sublime-text-3/Packages/User/Preferences.sublime-settings &>> $LOG
echo '// Settings in here override those in "Default/Preferences.sublime-settings",
// and are overridden in turn by syntax-specific settings.
{
        "hot_exit": false,
        "remember_open_files": false
}
' > $USER_HOMEDIR/.config/sublime-text-3/Packages/User/Preferences.sublime-settings 2>> $LOG

chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.config/sublime-text-3 &>> $LOG
chmod 664 $USER_HOMEDIR/.config/sublime-text-3/Packages/User/Preferences.sublime-settings &>> $LOG

echo -e "\n#########################\n" >> $LOG

# Installing more pkgs and repos
echo -e "\n${NCB}5. Installing remaining pkgs from git repo:${NC}" | tee -a $LOG

# install rar/unrar
echo -n "Rar/Unrar..." | tee -a $LOG
tar -xvzf $USER_HOMEDIR/git_repos/pkgs/rarlinux-x64-5.9.0.tar.gz -C /var/tmp/ &>> $LOG
cp -f /var/tmp/rar/rar /usr/local/bin/ &>> $LOG
cp -f /var/tmp/rar/unrar /usr/local/bin/ &>> $LOG

if [ `ls -l /usr/local/bin/*rar 2>> $LOG | wc -l` -eq '2' ]; then
	echo -e "${GREEN}DONE${NC}" | tee -a $LOG
else
	echo -e "${RED}FAILED${NC}" | tee -a $LOG
fi


# install chrome
echo -n "Google chrome..." | tee -a $LOG
yum -y install $USER_HOMEDIR/git_repos/pkgs/google-chrome-stable_current_x86_64.rpm &>> $LOG

if [ `rpm -qa | grep -i google-chrome | wc -l` -eq '0' ]; then
	echo -e "${RED}FAILED${NC}" | tee -a $LOG
else
	echo -e "${GREEN}DONE${NC}" | tee -a $LOG
fi


# install VNC
echo -n "VNC..." | tee -a $LOG
yum -y install $USER_HOMEDIR/git_repos/pkgs/VNC-Viewer-6.20.113-Linux-x64.rpm &>> $LOG

if [ `rpm -qa | grep -i realvnc | wc -l` -eq '0' ]; then
	echo -e "${RED}FAILED${NC}" | tee -a $LOG
else
	echo -e "${GREEN}DONE${NC}" | tee -a $LOG
fi

# install Foxit interactively
#echo -n "Foxit (PDF reader) is now ready for interactive installation..." | tee -a $LOG
#chmod +x $USER_HOMEDIR/git_repos/pkgs/FoxitReader.enu.setup.2.4.4.0911.x64.run &>> $LOG
#$USER_HOMEDIR/git_repos/pkgs/FoxitReader.enu.setup.2.4.4.0911.x64.run &>> $LOG && (echo -e "${GREEN}DONE${NC}" | tee -a $LOG) || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
#echo | tee -a

# install Adobe reader
echo -n "Adobe reader..." | tee -a $LOG
rpm -ivh $USER_HOMEDIR/git_repos/pkgs/AdbeRdr9.5.5-1_i486linux_enu.rpm &>> $LOG

if [ `rpm -qa | grep -i adobereader | wc -l` -eq '0' ]; then
	echo -e "${RED}FAILED${NC}" | tee -a $LOG
else
	echo -e "${GREEN}DONE${NC}" | tee -a $LOG
fi

MIME_TYPES=`grep -i evince.desktop /usr/share/applications/mimeapps.list | cut -d= -f1`
if [ -n "$MIME_TYPES" ]; then
	for MIME in $MIME_TYPES; do
		su - $USER_NAME -c "gio mime $MIME AdobeReader.desktop" &>> $LOG
	done
fi

echo | tee -a


# extract office
echo | tee -a
echo  "Preparing system for Microsoft Office 2007 installation..." | tee -a $LOG
echo 
echo -e "If you get a prompt to install ${NCB}Wine Mono${NC} / ${NCB}Wine Gecko${NC} packages, it's suggested to hit ${REDB}CANCEL${NC}" | tee -a $LOG
echo -e "When MS Office installation wizard starts, select ${GREEN}INSTALL NOW${NC}, i.e ${REDB}do NOT click CUSTOMIZE${NC}" | tee -a $LOG
tar -xvzf $USER_HOMEDIR/git_repos/pkgs/office_2007_enterprise_32bit.tar.gz -C /var/tmp/ &>> $LOG
chown -R $USER_NAME.$USER_NAME /var/tmp/office_2007_enterprise_32bit &>> $LOG


# install custom wine
mkdir /opt/wine4 &>> $LOG
tar -xvzf $USER_HOMEDIR/git_repos/pkgs/PlayOnLinux-wine-4.0-upstream-linux-x86.tar.gz -C /opt/wine4/ &>> $LOG || (echo -e "${RED}FAILED${NC}" | tee -a $LOG)
echo 'export PATH=/opt/wine4/bin:$PATH' >> $USER_HOMEDIR/.bashrc
echo 'export LD_LIBRARY_PATH=/opt/wine4/lib:/opt/wine4/lib/wine' >> $USER_HOMEDIR/.bashrc
echo 'export WINEDLLOVERRIDES="winemenubuilder.exe=d"' >> $USER_HOMEDIR/.bashrc
echo 'export WINESERVER=/opt/wine4/bin/wineserver' >> $USER_HOMEDIR/.bashrc
echo 'export WINELOADER=/opt/wine4/bin/wine' >> $USER_HOMEDIR/.bashrc
echo 'export WINEDLLPATH=/opt/wine4/lib/wine' >> $USER_HOMEDIR/.bashrc

export PATH=/opt/wine4/bin:$PATH &>> $LOG
export LD_LIBRARY_PATH=/opt/wine4/lib:/opt/wine4/lib/wine &>> $LOG
export WINEDLLOVERRIDES="winemenubuilder.exe=d" &>> $LOG
export WINESERVER=/opt/wine4/bin/wineserver &>> $LOG
export WINELOADER=/opt/wine4/bin/wine &>> $LOG
export WINEDLLPATH=/opt/wine4/lib/wine &>> $LOG

chmod +x /opt/wine4/bin &>> $LOG

# configure .wine, basically it will create/setup ~/.wine folder
#su - $USER_NAME -c /opt/wine4/bin/winecfg &>> $LOG

# create .wine folder
mkdir $USER_HOMEDIR/.wine &>> $LOG
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.wine &>> $LOG

# copy Word, Excel app icons
mkdir $USER_HOMEDIR/.wine/msoffice_icons &>> $LOG
cp $USER_HOMEDIR/git_repos/fc31_conf/msoffice_icons/* $USER_HOMEDIR/.wine/msoffice_icons/ &>> $LOG
chown -R $USER_NAME.$USER_NAME $USER_HOMEDIR/.wine/msoffice_icons &>> $LOG

# copy wrapper scripts
cp $USER_HOMEDIR/git_repos/fc31_conf/msoffice/*sh /usr/local/bin/ &>> $LOG
chmod +x /usr/local/bin/wordwrapper.sh &>> $LOG
chmod +x /usr/local/bin/excelwrapper.sh &>> $LOG
chown $USER_NAME.$USER_NAME /usr/local/bin/wordwrapper.sh &>> $LOG
chown $USER_NAME.$USER_NAME /usr/local/bin/excelwrapper.sh &>> $LOG

# copy .desktop files
cp $USER_HOMEDIR/git_repos/fc31_conf/msoffice/*desktop $USER_HOMEDIR/.local/share/applications/ &>> $LOG
chown $USER_NAME.$USER_NAME $USER_HOMEDIR/.local/share/applications/word.desktop &>> $LOG
chown $USER_NAME.$USER_NAME $USER_HOMEDIR/.local/share/applications/excel.desktop &>> $LOG

# setting word, excel as default apps for doc and xls file types
su - $USER_NAME -c "gio mime application/vnd.ms-word word.desktop" &>> $LOG
su - $USER_NAME -c "gio mime application/vnd.openxmlformats-officedocument.wordprocessingml.document word.desktop" &>> $LOG
su - $USER_NAME -c "gio mime application/vnd.openxmlformats-officedocument.wordprocessingml.template word.desktop" &>> $LOG

su - $USER_NAME -c "gio mime application/vnd.ms-excel excel.desktop" &>> $LOG
su - $USER_NAME -c "gio mime application/vnd.openxmlformats-officedocument.spreadsheetml.sheet excel.desktop" &>> $LOG
su - $USER_NAME -c "gio mime application/vnd.openxmlformats-officedocument.spreadsheetml.template excel.desktop" &>> $LOG

# change HOMEDIR to actual homedir of running user (ms office wrapper scripts)
#sed -i "s/USER_NAME/$USER_NAME/g" /usr/local/bin/wordwrapper.sh &>> $LOG
#sed -i "s/USER_NAME/$USER_NAME/g" /usr/local/bin/excelwrapper.sh &>> $LOG

# change HOMEDIR to actual homedir of running user (.desktop ms office files)
sed -i "s/USER_NAME/$USER_NAME/g" $USER_HOMEDIR/.local/share/applications/word.desktop &>> $LOG
sed -i "s/USER_NAME/$USER_NAME/g" $USER_HOMEDIR/.local/share/applications/excel.desktop &>> $LOG

# run office setup.exe
echo
echo "Starting Microsoft Office installation..."
echo -e "${NCB}**** OFFICE 2007 KEY: `cat $USER_HOMEDIR/git_repos/pkgs/office_2007_key` ****${NC}" | tee -a $LOG
echo
su - $USER_NAME -c "env DISPLAY=:0 WINEPREFIX=$USER_HOMEDIR/.wine /opt/wine4/bin/wine /var/tmp/office_2007_enterprise_32bit/Setup/setup.exe" &>> $LOG

# remove any .desktop entries, that might have been installed by wine, bcz it will be mixed up with my custom shortcuts
rm -rf $USER_HOMEDIR/.local/share/applications/wine* &>> $LOG

# remove keyring to stop "keyring password needed" popup window, when i open firefox/chrome
#rm $USER_HOMEDIR/.local/share/keyrings/*keyring &>> $LOG

echo | tee -a $LOG
echo | tee -a $LOG

echo -e "${NCB}****** LiMOS Desktop environment configuration complete. Restart PC and login to openbox. Enjoy and don't forget...Less is More !!! ******${NC}" | tee -a $LOG
echo
